const sgMail = require("@sendgrid/mail");
require("dotenv").config();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);
Users = require("./usersModel");

// Get All Users
exports.getUsers = function (req, res) {
  Users.get(function (err, users) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    res.json({
      status: "success",
      message: "Users list retrieved successfully",
      data: users,
    });
  });
};

// Logged in users
exports.postLoggedInUsers = function (req, res) {
  Users.count({ email: req.body.email }, function (err, count) {
    if (count == 0) {
      // document doesnt exists
      var users = new Users();
      users.name = req.body.name;
      users.email = req.body.email;
      // save the users and check for errors
      users.save(function (err) {
        if (err) res.json(err);
        res.json({
          message: "Stored Logged In User Data",
        });
      });
      // send an sign up confirmation email
      const msg = {
        to: users.email,
        from: "tech.analogies.2021@gmail.com",
        subject: "Hi " + users.name + " Thank you for joining Tech Analogy",
        text: "TBD - email template",
      };
      sgMail
        .send(msg)
        .then(() => {
          console.log("Email sent");
          res.json({
            status: "success",
            message: "Email sent",
          });
        })
        .catch((error) => {
          console.error(error);
          res.json({
            status: "Error",
          });
        });
    } else {
      res.json({
        message: "user already exists",
      });
    }
  });
};

// Email New Term for Admin Review
exports.emailTermForReview = function (req, res) {
  // send an email to acknowledge the suggested term recieved
  const msg = {
    to: req.body.email,
    from: "tech.analogies.2021@gmail.com",
    subject: "Thank you for submitting " + req.body.term + " for review",
    text: "TBD - email template",
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log("Email sent");
      res.json({
        status: "success",
        message: "Email sent",
      });
    })
    .catch((error) => {
      console.error(error);
      res.json({
        status: "Error",
      });
    });
};
