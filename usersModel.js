var mongoose = require("mongoose");

var usersSchema = mongoose.Schema({
  email: {
    type: String,
  },
  name: {
    type: String,
  },
});
// Export users model
var users = (module.exports = mongoose.model("users", usersSchema));

module.exports.get = function (callback, limit) {
  users.find(callback).limit(limit);
};
