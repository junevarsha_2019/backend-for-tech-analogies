let router = require("express").Router();

// Set default API response
router.get("/", function (req, res) {
  res.json({
    status: "/api/ route is working",
    message: "Welcome to the Tech Analogies API",
  });
});

var techAnalogyController = require("./techAnalogyController");
var userController = require("./usersController");
var userHistoryController = require("./usersHistoryController");

router
  .route("/analogies")
  .get(techAnalogyController.index)
  .post(techAnalogyController.new);

router
  .route("/analogies/:_id")
  .get(techAnalogyController.getTermDefinition)
  .put(techAnalogyController.createDefinition);

router
  .route("/users")
  .get(userController.getUsers)
  .post(userController.postLoggedInUsers);

router.route("/users/sendAck").post(userController.emailTermForReview);

router
  .route("/usersHistory")
  .get(userHistoryController.getUsersHistory)
  .post(userHistoryController.postUserHistory);

module.exports = router;
