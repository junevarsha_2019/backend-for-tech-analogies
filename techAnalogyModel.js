var mongoose = require("mongoose");
// Setup schema
var childSchema = mongoose.Schema({
  beginner: { type: String },
  advanced: { type: String },
});

var techAnalogySchema = mongoose.Schema({
  _id: {
    type: String,
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  create_date: {
    type: Date,
    default: Date.now,
  },
  beginner: {
    type: String,
  },
  advanced: {
    type: String,
  },
});
// Export techAnalogy model
var techAnalogy = (module.exports = mongoose.model(
  "analogies",
  techAnalogySchema
));

module.exports.get = function (callback, limit) {
  techAnalogy.find(callback).limit(limit);
};
