var mongoose = require("mongoose");

var usersHistorySchema = mongoose.Schema({
  email: {
    type: String,
  },
  time: { type: Date, default: Date.now },
});

// Export users model
var usersHistory = (module.exports = mongoose.model(
  "usersHistory",
  usersHistorySchema
));

module.exports.get = function (callback, limit) {
  usersHistory.find(callback).limit(limit);
};
