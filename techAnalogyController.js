TechAnalogy = require("./techAnalogyModel");

// Handle index actions
exports.index = function (req, res) {
  TechAnalogy.get(function (err, techAnalogies) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    res.json({
      status: "success",
      message: "Tech Analogies list retrieved successfully",
      data: techAnalogies,
    });
  });
};
// Handle create techAnalogy actions
exports.new = function (req, res) {
  var techAnalogy = new TechAnalogy();
  techAnalogy.name = req.body.name ? req.body.name : techAnalogy.name;
  techAnalogy._id = req.body._id;
  // save the techAnalogy and check for errors
  techAnalogy.save(function (err) {
    if (err) res.json(err);
    res.json({
      message: "New techAnalogy created!",
      data: techAnalogy,
    });
  });
};

exports.createDefinition = function (req, res) {
  // var techAnalogy = new TechAnalogy();
  console.log(req.params._id);
  TechAnalogy.findOneAndUpdate(
    { _id: req.params._id },
    { beginner: req.body.beginner, advanced: req.body.advanced },
    { upsert: true, new: true },
    function (err, dashboard) {
      if (err) res.send(err);
      res.send(dashboard);
    }
  );
};

exports.getTermDefinition = function (req, res) {
  TechAnalogy.find({ _id: req.params._id }, function (err, definitions) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    res.json({
      status: "success",
      message: "Definitions retrieved successfully",
      data: definitions,
    });
  });
};
