let express = require("express");
let bodyParser = require("body-parser");
let mongoose = require("mongoose");
let apiRoutes = require("./api-routes");

let app = express();
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "*");
  res.setHeader("Access-Control-Allow-Headers", "*");
  next();
});
mongoose.connect("mongodb://localhost/analogiesdb", { useNewUrlParser: true });
var db = mongoose.connection;
if (!db) console.log("Error connecting db");
else console.log("Db connected successfully");

var port = process.env.PORT || 8083;
app.get("/", (req, res) => res.send("Tech Analogies"));

// Use Api routes in the App
app.use("/api", apiRoutes);

// Launch app to listen to specified port
app.listen(port, function () {
  console.log("Running tech analogies app on port " + port);
});
