UsersHistory = require("./usersHistoryModel");

// Get All Users
exports.getUsersHistory = function (req, res) {
  UsersHistory.get(function (err, users) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    res.json({
      status: "success",
      message: "History list retrieved successfully",
      data: users,
    });
  });
};
// Logged in users
exports.postUserHistory = function (req, res) {
  var usersHistory = new UsersHistory();
  usersHistory.email = req.body.email;
  // save the users and check for errors
  usersHistory.save(function (err) {
    if (err) res.json(err);
    res.json({
      message: "Stored User History Data",
    });
  });
};
